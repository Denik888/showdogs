const reverseButton = document.querySelector('.reversebutton');

const dogImages = document.querySelector('.show-dogs__images');

  reverseButton.addEventListener('click', function(event){

    // через async/await

    // async function getResponce() {
    //   const responce = await fetch('https://dog.ceo/api/breeds/image/random', {credentials: "omit"});
    //   const content = await responce.json();
    //   const images = content.message;
    //   const showImages = `<img src="${images}" alt="">`;
    //   dogImages.innerHTML = showImages;
    // }

    // getResponce();

    // через .then

    fetch('https://dog.ceo/api/breeds/image/random')
    .then(response => response.json())
    .then(showImages => dogImages.innerHTML = `<img src="${showImages.message}" alt="">`);
  
  });